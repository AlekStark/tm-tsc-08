package com.tsc.afedorovkaritsky.tm;

import com.tsc.afedorovkaritsky.tm.api.ICommandRepository;
import com.tsc.afedorovkaritsky.tm.constant.ArgumentConst;
import com.tsc.afedorovkaritsky.tm.constant.TerminalConst;
import com.tsc.afedorovkaritsky.tm.model.Command;
import com.tsc.afedorovkaritsky.tm.repository.CommandRepository;
import com.tsc.afedorovkaritsky.tm.util.NumberUtil;

import java.util.*;

public class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public static void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.INFO:
                showInfo();
                break;
            case TerminalConst.COMMANDS:
                showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                showArguments();
                break;

            case TerminalConst.EXIT:
                exit();
                break;
            default:
                showErrorCommand();
        }
    }

    private static void showErrorArgument() {
        System.err.println("Ошибка! Аргумент не поддерживается...");
        System.exit(1);
    }

    private static void showErrorCommand() {
        System.err.println("Ошибка! Команда не найдена...");
    }

    public static void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            default:
                showErrorArgument();
        }
    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public static void exit() {
        System.exit(0);
    }

    public static void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Aleksandr Fedorov-Karitsky");
        System.out.println("Email: afedorovkaritsky@tsconsulting.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0.0");
    }

    public static void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) showCommandValue(command.getName());
    }

    public static void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) showCommandValue(command.getArgument());
    }

    private static void showCommandValue(final String value) {
        if (value == null || value.isEmpty()) return;
        System.out.println(value);
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }
}
